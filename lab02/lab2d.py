import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from random import randrange
import math

#################################################
delta = 0.1
x_size = 10
y_size = 10
ko = 1
do = 20
kp = 1
liczba_przeszkod = 4
dr = 5

#################################################
class Vect:
    def __init__(self,a,b):
        self.X = a
        self.Y = b
    def see(self):
        print("[",self.X,", ",self.Y,"]")
         
def randomvect():
    a = randrange(-x_size, x_size)
    b = randrange(-y_size, y_size)
    v = Vect(a, b)
    return v
    
def stworzprzeszkody(n):
    p = np.arange(n, dtype=Vect)
    for i in range(0,n):
        p[i] = randomvect()
    return p

def Fp(r,c):
    a = np.sqrt(math.pow(r.X - c.X,2) + math.pow(r.Y - c.Y,2))
    F = kp * a
    return F

def Foi(r,p):
    s = 0
    i = 0
    for foi in p:
        a = np.sqrt(math.pow(r.X - foi.X,2) + math.pow(r.Y - foi.Y,2))
        if a == 0:
            return -20
        if a <= do:
            b = ((-koi[i])*((1/a)-(1/do))*(1/(a**2)))
            if b < -1:
                return -20
            else:
                s += b
        else:
            return 0
        i += 1
    return s

def Fw(r,c,p):
    return (Fp(r,c) - Foi(r,p))

def macierzSil():
    dokladnosc = int(20/delta+1)
    F = np.ones((dokladnosc, dokladnosc))
    for a in range(0,dokladnosc):
        for b in range(0,dokladnosc):
            r = Vect(-x_size + b*delta,-y_size + a*delta)
            F[a][b] = Fw(r,finish,przeszkody)
    return F

def zonaP(r):
    F = np.arange(8, dtype=Vect)
    for i in F:
        F[i] = Vect(r.X, r.Y)
    # 0-gora; 1-prawa gora; 2-prawo; 3-prawy dol
    # 4-dol; 5-lewy dol; 6-lewo; 7-lewa gora
    for i in range(0,8):
        if i == 0 or i == 1 or i == 7:
            F[i].Y += delta
            F[i].Y = round(F[i].Y*10)/10
        if i == 3 or i == 4 or i == 5:
            F[i].Y -= delta
            F[i].Y = round(F[i].Y*10)/10
        if i == 1 or i == 2 or i == 3:
            F[i].X += delta
            F[i].X = round(F[i].X*10)/10
        if i == 5 or i == 6 or i == 7:
            F[i].X -= delta
            F[i].X = round(F[i].X*10)/10
    return F

def nextStep(r):
    W = zonaP(r)
    F = np.zeros((8,1))
    m = 1000
    NS = Vect(-100,-100)
    for i in range(0,8):
        if W[i].X >= -10 and W[i].X <= 10 and W[i].Y >= -10 and W[i].Y <= 10:
            a = round((W[i].Y + y_size)/delta)
            b = round((W[i].X + x_size)/delta)
            F[i] = Z[a][b]
            if F[i] <= m:
                NS = Vect(W[i].X, W[i].Y)
                m = F[i]
        else:
            F[i] = 1000
    #print('m: ',m)
    return NS

def road():
    n = 5000
    koniec = 0;
    pozycja = np.arange(n, dtype=Vect)
    pozycja[0] = Vect(start.X, start.Y)
    for i in range(1,n):
        pozycja[i] = nextStep(pozycja[i-1])
        #pozycja[i].see()
        #finish.see()
        if pozycja[i].X == finish.X and pozycja[i].Y == finish.Y:
            koniec = i
            break
    if koniec != 0:
        tab = np.arange(n - koniec)
        for t in tab:
            tab[t] = koniec+t     
        pozycja = np.delete(pozycja,tab,0)
    return pozycja
        
def init_macierzZ():
    dokladnosc = int(20/delta+1)
    F = np.zeros((dokladnosc, dokladnosc))
    for a in range(0,dokladnosc):
        for b in range(0,dokladnosc):
            F[a][b] = -1
    return F

def silywokolo(r):
    dokladnosc = int(20/delta+1)
    for a in range(0,dokladnosc):
        for b in range(0,dokladnosc):
            punkt = Vect(-x_size + b*delta,-y_size + a*delta)
            odl = np.sqrt(math.pow(r.X - punkt.X,2) + math.pow(r.Y - punkt.Y,2))
            #print(odl)
            if odl <= dr:
                if Z[a][b] == -1:
                    Z[a][b] = Fw(punkt,finish,przeszkody)

def droga():
    n = 5000
    koniec = 0;
    pozycja = np.arange(n, dtype=Vect)
    pozycja[0] = Vect(start.X, start.Y)
    for i in range(1,n):
        print(i)
        silywokolo(pozycja[i-1])
        pozycja[i] = nextStep(pozycja[i-1])
        if pozycja[i].X == finish.X and pozycja[i].Y == finish.Y:
            koniec = i
            break
    if koniec != 0:
        tab = np.arange(n - koniec)
        for t in tab:
            tab[t] = koniec+t     
        pozycja = np.delete(pozycja,tab,0)
    return pozycja
    
    
    
    
#####################################################
przeszkody = stworzprzeszkody(liczba_przeszkod)
start = Vect(-10,randrange(-y_size, y_size))
finish = Vect(10,randrange(-y_size, y_size))
koi = np.ones((liczba_przeszkod,1))

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = init_macierzZ()

#sciezka = road()
sciezka = droga()


fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])
plt.plot(start.X, start.Y, "or", color='blue')
plt.plot(finish.X, finish.Y, "or", color='red')
for obstacle in przeszkody:
    plt.plot(obstacle.X, obstacle.Y, "or", color='black')
for s in sciezka:
    plt.plot(s.X, s.Y, "or", color='green')
plt.colorbar(orientation='vertical')
plt.grid(True)
plt.show()