import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from random import randrange
import math

class Vect:
	def __init__(self,a,b):
		self.X = a
		self.Y = b
	def see(self):
		print("[",self.X,", ",self.Y,"]")
		 
def randomvect():
	a = randrange(-x_size, x_size)
	b = randrange(-y_size, y_size)
	v = Vect(a, b)
	return v
	
def stworzprzeszkody(n):
	p = np.arange(n, dtype=Vect)
	for i in range(0,n):
		p[i] = randomvect()
	return p

def Fp(r,c): # r - pozycja robota; c - pozycja celu
	a = math.sqrt(math.pow(r.X - c.X,2) + math.pow(r.Y - c.Y,2))
	F = kp*a
	return F

def Foi(r,p,i): # r - pozycja robota; p - pozycja przeszkody
	a = math.sqrt(math.pow(r.X - p.X,2) + math.pow(r.Y - p.Y,2))
	if(a == 0):
		return math.inf
	if(a <= do):
		F = -koi[i] * ((1/a) - (1/do)) * (1/math.pow(a,2))
	else:
		F = 0
	return F

def obliczF(r,c,p,l):
	w = 0
	for i in range(0,l):
		w += Foi(r,p[i],i)
	F = Fp(r,c) - w
	return F

def macierzsil():
	dokladnosc = int(20/delta+1)
	#print(dokladnosc)
	F = np.ones((dokladnosc, dokladnosc))
	for a in range(0,dokladnosc):
		for b in range(0,dokladnosc):
			r = Vect(-x_size + b*delta,-y_size + a*delta)
			F[a][b] = obliczF(r,finish,przeszkody,liczba_przeszkod) 
		#print(dokladnosc-a)
	return F

def Up(r,c):
	a = math.sqrt(math.pow(r.X - c.X,2) + math.pow(r.Y - c.Y,2))
	U = 0.5*kp*math.pow(a,2)
	return U

def Voi(r,p,i):
	a = math.sqrt(math.pow(r.X - p.X,2) + math.pow(r.Y - p.Y,2))
	if(a == 0):
		return math.inf
	if(a <= do):
		V = 0.5*koi[i]*math.pow((1/a)-(1/do),2)
	else:
		V = 0
	return V
	
def Uw(r,c,p,l):
	s = 0
	for i in range(0,l):
		s += Voi(r,p[i],i)
	U = Up(r,c) + s
	return U
	
def macierzpot():
	dokladnosc = int((20/delta)+1)
	print(dokladnosc)
	U = np.ones((dokladnosc, dokladnosc))
	for a in range(0,dokladnosc):
		for b in range(0,dokladnosc):
			r = Vect(-x_size + b*delta,-y_size + a*delta)
			U[a][b] = Uw(r,finish,przeszkody,liczba_przeszkod) 
		print(dokladnosc-a)
	return U

######################################################################

delta = 0.1
x_size = 10
y_size = 10
ko = 1
do = 20
kp = 100
liczba_przeszkod = 4

przeszkody = stworzprzeszkody(liczba_przeszkod)
start = Vect(-10,randrange(-y_size, y_size))
finish = Vect(10,randrange(-y_size, y_size))
koi = np.ones((4,1))
robot = Vect(start.X, start.Y)
x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = macierzpot()

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])
plt.plot(start.X, start.Y, "or", color='blue')
plt.plot(finish.X, finish.Y, "or", color='red')
for obstacle in przeszkody:
    plt.plot(obstacle.X, obstacle.Y, "or", color='black')
plt.colorbar(orientation='vertical')
plt.grid(True)
plt.show()
























